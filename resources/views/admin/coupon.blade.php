@extends('admin.inc.app')
@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            <div class="row mb-3">
                <div class="col-sm-6 mt-1 px-0">
                    <div class="iq-card mb-0 rounded-0">
                        <div class="iq-card-header d-flex justify-content-between">
                            <div class="iq-header-title">
                                <h4 class="card-title font-weight-bold">
                                    Edit Coupon
                                </h4>
                            </div>
                            <button data-toggle="modal" data-target="#delete" class="btn btn-danger d-block"><i class="fa fa-trash mr-0"></i></button>
                        </div>
                        <div class="iq-card-body">
                            <form method="POST" action="/admin/update-coupon" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" id="" value="{{$coupon->id}}">
                                <div class="form-group">
                                    <label for="fname">Code:</label>
                                    <input type="text" class="form-control" id="fname" name="code" value="{{$coupon->code}}">
                                </div>
                                <div class="input-group mb-3">
                                    <label for="percentage" class="col-12 px-0">Percentage:</label>
                                    <input type="text" id="percentage" class="form-control" value="{{$coupon->percentage}}" name="percentage" style="border-top-left-radius:10px;border-bottom-left-radius:10px;">
                                    <div class="input-group-append">
                                        <span class="input-group-text px-3" style="border-top-right-radius:9px;border-bottom-right-radius:9px;border:1px solid #d7dbda;background-color: #fff;">%</span>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary px-5 py-2">Update</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="delete" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content pb-3">
                <div class="modal-header border-0">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Delete Coupon</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <i class="fa fa-trash text-danger" style="font-size: 90px"></i>
                    <h5 class="mt-3 mb-0">Are you sure?</h5>
                    <h6 class="mb-3">Delete this coupon code</h6>
                    <form action="/admin/delete-coupon" method="POST">
                        @csrf
                        <input type="hidden" name="id" value="{{$coupon->id}}">
                        <button type="submit" class="btn btn-danger px-5 py-2">Delete</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
