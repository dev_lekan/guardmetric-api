@extends('admin.inc.app')
@section('track')
    active
@endsection
@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            <div class="d-flex justify-content-center">
                <form action="/admin/get-order" method="post" class="mt-5 text-center">
                    @include('admin.inc.notification')
                    @csrf
                    <div class="form-group" style="width: 33em">
                        <input type="text" placeholder="Enter track code..." class="form-control no-focus" name="code" style="background-color: white">
                    </div>
                    <button type="submit" class="btn btn-primary px-5">Search</button>
                </form>
            </div>
        </div>
    </div>
@endsection