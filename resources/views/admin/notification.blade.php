@extends('admin.inc.app')
@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            @include('admin.inc.notification')
            <div class="row">
                <div class="col-sm-12 px-0">
                    <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between align-items-center">
                            <div class="iq-header-title">
                                <h4 class="card-title font-weight-bold">Notification</h4>
                            </div>
                        </div>
                        <div class="iq-card-body pt-0">
                            <label for="">Name</label>
                            <h4 class="card-title">{{$msg->name}}</h4>
                            <label for="">Email</label>
                            <h4 class="card-title">{{$msg->email}}</h4>
                            <label for="">Body</label>
                            <p>{{$msg->body}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
