@extends('admin.inc.app')
@section('orders')
    active
@endsection
@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            @include('admin.inc.notification')
            <div class="row">
                <div class="col-lg-6 px-0">
                    <div class="iq-card">
                        <div class="iq-card-body">
                            <p>
                                <b>Order Details </b>
                                @if($order->status == 0)
                                    <span class="badge badge-primary font-size-12">unpaid</span>
                                @elseif($order->status == 1)
                                    <span class="badge badge-secondary font-size-12">paid</span>
                                @elseif($order->status == 2)
                                    <span class="badge badge-success font-size-12">delivered</span>
                                @else
                                    <span class="badge badge-danger font-size-12">cancelled</span>
                                @endif
                            </p>
                            <div class="d-flex justify-content-between">
                                <span>First name</span>
                                <span>{{$order->user->firstname}}</span>
                            </div>
                            <div class="d-flex justify-content-between">
                                <span>Last name</span>
                                <span class="text-success">{{$order->user->lastname}}</span>
                            </div>
                            <div class="d-flex justify-content-between">
                                <span>Email</span>
                                <span>{{$order->user->email}}</span>
                            </div>
                            <div class="d-flex justify-content-between">
                                <span>Reference</span>
                                <span>{{$order->reference}}</span>
                            </div>
                            <div class="d-flex justify-content-between">
                                <span>Phone</span>
                                <span class="text-amber">{{$order->user->phone}}</span>
                            </div>
                            <div class="d-flex justify-content-between">
                                <span>Address</span>
                                <span class="text-cobalt-blue">{{$order->address}}</span>
                            </div>
                            <div class="d-flex justify-content-between">
                                <span>State</span>
                                <span class="text-cyan">{{$order->user->state}}</span>
                            </div>
                            <div class="d-flex justify-content-between">
                                <span>City</span>
                                <span class="text-success">{{$order->user->city}}</span>
                            </div>
                            <div class="d-flex justify-content-between">
                                <span>Zip code</span>
                                <span class="text-primary">{{$order->user->zip}}</span>
                            </div>
                            <hr>
                            <div class="d-flex justify-content-between">
                                <span class="text-dark"><strong>Total</strong></span>
                                <span class="text-dark"><strong>₦{{$order->total}}</strong></span>
                            </div>
                            <div class="d-flex justify-content-between">
                                @if($order->status == 1)
                                    <a id="place-order" href="javascript:void();" data-toggle="modal" data-target="#deliver"  class="btn btn-success d-block mt-1">Deliver order</a>
                                    <a id="place-order" href="javascript:void();" data-toggle="modal" data-target="#cancel" class="btn btn-danger d-block mt-1">Cancel order</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 px-0 pl-lg-2">
                    <div class="iq-card">
                        <div class="iq-card-body">
                            <p><b>Items</b></p>
                            <div class="table-responsive">
                                <table class="table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            <th>Item</th>
                                            <th>Quantity</th>
                                            <th>Price</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($order['items'] as $item)
                                            <tr>
                                                <td>{{\App\Products::where('unique_id',$item->unique_id)->first()->name}}</td>
                                                <td>{{$item->quantity}}</td>
                                                <td>₦{{$item->price}}</td>
                                                <td>₦{{ $item->quantity * $item->price }}</td>
                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="cancel" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content pb-3">
                            <div class="modal-header border-0">
                                <h5 class="modal-title" id="exampleModalCenterTitle">Cancel order</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body text-center">
                                <i class="fa fa-warning text-warning" style="font-size: 90px"></i>
                                <h5 class="my-3">Are you sure?</h5>
                                <form action="/admin/cancel-order" method="POST">
                                    @csrf
                                    <input type="hidden" name="order_id" value="{{$order->id}}">
                                    <button type="submit" class="btn btn-primary px-5 py-2">Cancel</button>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="deliver" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content pb-3">
                            <div class="modal-header border-0">
                                <h5 class="modal-title" id="exampleModalCenterTitle">Deliver order</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body text-center">
                                <i class="fa fa-check text-success" style="font-size: 90px"></i>
                                <h5 class="my-3">Are you sure?</h5>
                                <form action="/admin/deliver-order" method="POST">
                                    @csrf
                                    <input type="hidden" name="order_id" value="{{$order->id}}">
                                    <button type="submit" class="btn btn-success px-5 py-2">Deliver order</button>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
