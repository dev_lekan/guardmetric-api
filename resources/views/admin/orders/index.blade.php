@extends('admin.inc.app')
@section('orders')
    active
@endsection
@section('ord')
    active
@endsection
@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            @include('admin.inc.notification')
            <div class="row">
                <div class="col-sm-12 px-0">
                    <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between align-items-center">
                            <div class="iq-header-title">
                                <h4 class="card-title font-weight-bold">Orders</h4>
                            </div>
                        </div>
                        <div class="iq-card-body pt-0">
                            <div class="table-responsive">
                                <table id="datatableF" class="table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            <th>First name</th>
                                            <th>Last name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Address</th>
                                            <th>Date</th>
                                            <th>Status</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($orders as $order)
                                            <tr onclick="window.location.href='/admin/order/{{$order->id}}'">
                                                <td>{{$order->user->firstname}}</td>
                                                <td>{{$order->user->lastname}}</td>
                                                <td>{{$order->user->email}}</td>
                                                <td>{{$order->user->phone}}</td>
                                                <td>{{$order->address}}</td>
                                                <td>{{$order->created_at->format('d, M Y H:i a')}}</td>
                                                <td>
                                                    @if($order->status == 1)
                                                        <span class="badge badge-primary font-size-12">paid</span>
                                                    @elseif($order->status == 2)
                                                        <span class="badge badge-success font-size-12">delivered</span>
                                                    @else
                                                        <span class="badge badge-danger font-size-12">cancelled</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="/admin/order/{{$order->id}}" class="btn btn-primary">
                                                        <i class="fa fa-arrow-right mr-0"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>First name</th>
                                            <th>Last name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Address</th>
                                            <th>Date</th>
                                            <th>Status</th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
