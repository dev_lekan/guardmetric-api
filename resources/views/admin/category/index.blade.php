@extends('admin.inc.app')
@section('category')
    active
@endsection
@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            @include('admin.inc.notification')
            <div class="row">
                <div class="col-sm-12 px-0">
                    <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between align-items-center">
                            <div class="iq-header-title">
                                <h4 class="card-title font-weight-bold">Categories</h4>
                            </div>
                            <div class="iq-header-title">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addCategory">
                                    Add Category
                                </button>
                            </div>
                        </div>
                        <div class="iq-card-body pt-0">
                            <div class="table-responsive">
                                <table id="datatable" class="table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($category as $cat)
                                            <tr onclick="window.location.href='/admin/category/{{$cat->id}}'">
                                                <td>{{$cat->name}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Name</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="addCategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-modal="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle">Add Category</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="post" action="/admin/add-category" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="name">Name:</label>
                                    <input type="text" class="form-control" id="name" name="name">
                                </div>
                                <div class="form-group">
                                    <label for="image">Images:</label>
                                    <input type="file" name="image" class="form-control line-height-2">
                                </div>
                                <button type="submit" class="btn btn-primary px-5 py-2">Add</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
