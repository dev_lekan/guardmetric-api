@extends('admin.inc.app')
@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid px-0">
            <div class="student-tabs pt-3 pb-4 pl-3">
                <ul class="d-flex nav nav-pills">
                    <li>
                        <a class="nav-link active" data-toggle="pill" href="#personal-information">
                            Product
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" data-toggle="pill" href="#images">
                            Images
                        </a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="iq-edit-list-data">
                        <div class="tab-content">
                            @include('admin.inc.notification')
                            <div class="tab-pane fade active show" id="personal-information" role="tabpanel">
                                <div class="iq-card">
                                    <div class="iq-card-header d-flex justify-content-between">
                                        <div class="iq-header-title">
                                            <h4 class="card-title font-weight-bold">
                                                Edit Product
                                                @if($product->status == 0)
                                                    <span class="badge badge-danger font-size-12 py-0">Out of stock</span>
                                                @endif
                                            </h4>
                                        </div>
                                        <div>
                                            <form action="/admin/delete-product" method="POST">
                                                @csrf
                                                <input type="hidden" name="product_id" value="{{$product->id}}">
                                                <button class="btn btn-danger" type="submit">Delete <i class="fa fa-trash"></i> </button>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="iq-card-body">
                                        <div class="d-flex mb-2">
                                            @foreach (json_decode($product->colors) as $key=> $color)
                                                <form action="/admin/remove-product-color" id="remColor{{$key}}" method="POST">
                                                    @csrf
                                                    <input type="hidden" name="product_id" value="{{$product->id}}">
                                                    <input type="hidden" name="color" value="{{$color}}">
                                                    <div class="rec-color clickable" style="background:{{$color}}; width: 35px; height: 35px;border-color: transparent">
                                                        <a href="#" class="btn color-btn d-flex justify-content-center align-items-center" onclick="document.getElementById('remColor{{$key}}').submit()">
                                                            <i class="fa fa-trash mr-0"></i>
                                                        </a>
                                                    </div>
                                                </form>
                                            @endforeach
                                        </div>
                                        <form method="POST" action="/admin/update-product" id="updateProd" enctype="multipart/form-data">
                                            @csrf
                                            <input type="hidden" name="product_id" id="" value="{{$product->id}}">
                                            <div class="form-group">
                                                <label for="fname">Name:</label>
                                                <input type="text" class="form-control" id="fname" name="name" value="{{$product->name}}">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1">Category</label>
                                                <select class="form-control" id="exampleFormControlSelect1" name="category_id">
                                                    @foreach($categories as $cat)
                                                        <option value="{{$cat->id}}" {{$product->category_id==$cat->id?'selected':''}} >{{$cat->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="input-group mb-3">
                                                <label for="price" class="col-12 px-0">Price:</label>
                                                <input type="text" id="price" class="form-control" value="{{$product->price}}" name="price" style="border-top-left-radius:10px;border-bottom-left-radius:10px;">
                                                <div class="input-group-append">
                                                    <span class="input-group-text px-3" style="border-top-right-radius:9px;border-bottom-right-radius:9px;border:1px solid #d7dbda;background-color: #fff;">₦</span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1" class="d-block">Colors:</label>
                                                <select class="form-control selectpicker" id="exampleFormControlSelect1" multiple data-live-search="true" name="colors[]">
                                                    @foreach($colors as $color)
                                                        <option value="{{$color['hex']}}" style="background:{{$color['hex']}}">
                                                            {{$color['name']}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="image">Images:</label>
                                                <input type="file" name="images[]" class="form-control line-height-2" multiple>
                                            </div>
                                            <div class="form-group">
                                                <label for="desp">Description:</label>
                                                <textarea class="form-control" style="line-height:19px" name="description">{!! $product->description !!}</textarea>
                                            </div>

{{--                                            <div class="form-group">--}}
{{--                                                <label for="desp">Features:</label>--}}
{{--                                                <textarea class="form-control"  id="desp" name="features" value="{!! $product->features !!}">{!! $product->features !!}</textarea>--}}
{{--                                            </div>--}}

                                            <div class="checkbox mb-3">
                                                <label><input type="checkbox" name="set" {{$product->status == 0 ? 'checked':''}} > Out of stock</label>
                                            </div>
                                            <button type="submit" class="btn btn-primary px-5 py-2" onclick="document.getElementById('updateProd').submit()">Update</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="images" role="tabpanel">
                                <div class="row mx0">
                                    @foreach($product->images as $img)
                                        <div class="col-sm-3 mx-0 product-img mt-md-0 mt-3" style="">
                                            <img src="{{$img->name}}" alt="" style="height: 20em;width: 100%;object-fit: contain;">
                                            <form action="/admin/delete-product-image" method="POST">
                                                @csrf
                                                <input type="hidden" name="id" value="{{$img->id}}">
                                                <div class="form-group img-del">
                                                    <button type="submit" class="btn"><i class="fa fa-trash text-danger mr-0"></i></button>
                                                </div>
                                            </form>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
