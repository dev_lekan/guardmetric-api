@extends('admin.inc.app')
@section('products')
    active
@endsection
@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            @include('admin.inc.notification')
            <div class="row">
                <div class="col-sm-12 px-0">
                    <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between align-items-center">
                            <div class="iq-header-title">
                                <h4 class="card-title font-weight-bold">Products</h4>
                            </div>
                            <div class="iq-header-title">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addProduct">
                                    Add Product
                                </button>
                            </div>
                        </div>
                        <div class="iq-card-body pt-0">
                            <div class="table-responsive">
                                <table id="datatable" class="table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Price</th>
                                            <th>Category</th>
                                            <th>Quantity Available</th>
                                            <th>Colors</th>
                                            <th>Featured</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($products as $prod)
                                            <tr>
                                                <td>{{$prod->name}}</td>
                                                <td>₦{{$prod->price}}</td>
                                                <td>{{$prod['category']?$prod['category']['name']:''}}</td>
                                                <td>{{$prod->quantity}}</td>
                                                <td>
                                                    <div class="d-flex">
                                                        @if($prod->colors != null)
                                                            @foreach (json_decode($prod->colors) as $color)
                                                                <div class="rec-color" style="background:{{$color}}" >
                                                                </div>
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                </td>
                                                <td>
                                                    <form action="/admin/feature-product" method="POST" id="form{{$prod->id}}">
                                                        @csrf
                                                        <input type="hidden" name="product_id" value="{{$prod->id}}">
                                                        <div class="toggle-btn input-group">
                                                            <label class="switch">
                                                                <input type="checkbox" {{$prod->featured == 1 ? 'checked' : ''}} name="status" onclick="document.getElementById('form{{$prod->id}}').submit()">
                                                                <span class="slider round"></span>
                                                            </label>
                                                        </div>
                                                    </form>
                                                </td>
                                                <td>
                                                    <a class="btn btn-primary clickable" href="/admin/product/{{$prod->id}}"><i class="fa fa-arrow-right mr-0" style="font-size: 17px"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Name</th>
                                            <th>Price</th>
                                            <th>Category</th>
                                            <th>Quantity Available</th>
                                            <th>Colors</th>
                                            <th>Featured</th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="addProduct" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-modal="true">
                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle">Add Product</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="post" action="/admin/add-product" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="name">Name:</label>
                                    <input type="text" class="form-control" id="name" name="name">
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Category</label>
                                    <select class="form-control" id="exampleFormControlSelect1" name="category_id">
                                        <option selected="" disabled="">Select category</option>
                                        @foreach($categorys as $cat)
                                            <option value="{{$cat->id}}">{{$cat->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1" class="d-block">Colors:</label>
                                    <select class="form-control selectpicker" id="exampleFormControlSelect1" multiple data-live-search="true" name="colors[]">
                                       @foreach($colors as $color)
                                           <option value="{{$color['hex']}}" style="background:{{$color['hex']}}">
                                               {{$color['name']}}
                                           </option>
                                       @endforeach
                                    </select>
                                </div>
                                <div class="input-group mb-3">
                                    <label for="price" class="col-12 px-0">Price:</label>
                                    <input type="text" id="price" class="form-control" name="price" style="border-top-left-radius:10px;border-bottom-left-radius:10px;">
                                    <div class="input-group-append">
                                        <span class="input-group-text px-3" style="border-top-right-radius:9px;border-bottom-right-radius:9px;border:1px solid #d7dbda;background-color: #fff;">₦</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="quantity">Quantity available:</label>
                                    <input type="number" id="quantity" name="quantity" class="form-control line-height-2">
                                </div>
                                <div class="form-group">
                                    <label for="image">Images:</label>
                                    <input type="file" name="images[]" class="form-control line-height-2" multiple>
                                </div>
                                <div class="form-group">
                                    <label for="">Description:</label>
                                    <textarea class="form-control" id="descp" name="description" rows="6" style="line-height: 19px"></textarea>
                                </div>
{{--                                <div class="form-group">--}}
{{--                                    <label for="descp">Features:</label>--}}
{{--                                    <textarea class="" id="descp" name="features"></textarea>--}}
{{--                                </div>--}}
                                <button type="submit" class="btn btn-primary px-5 py-2">Add</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
