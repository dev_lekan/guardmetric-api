@extends('admin.inc.app')
@section('coupons')
    active
@endsection
@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            @include('admin.inc.notification')
            <div class="row">
                <div class="col-sm-12 px-0">
                    <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between align-items-center">
                            <div class="iq-header-title">
                                <h4 class="card-title font-weight-bold">Coupons</h4>
                            </div>
                            <div class="iq-header-title">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createCoupon">
                                    Create Coupon
                                </button>
                            </div>
                        </div>
                        <div class="iq-card-body pt-0">
                            <div class="table-responsive">
                                <table id="datatable" class="table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            <th>Code</th>
                                            <th>Percentage</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($coupons as $coupon)
                                        <tr>
                                            <td>{{$coupon->code}}</td>
                                            <td>{{$coupon->percentage}}%</td>
                                            <td>
                                                <a class="btn btn-primary clickable" href="/admin/coupon/{{$coupon->id}}"><i class="fa fa-arrow-right mr-0" style="font-size: 17px"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Code</th>
                                            <th>Percentage</th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="createCoupon" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-modal="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle">Create Coupon</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="post" action="/admin/create-coupon" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="name">Code:</label>
                                    <input type="text" class="form-control" id="name" name="code">
                                </div>
                                <div class="input-group mb-3">
                                    <label for="percentage" class="col-12 px-0">Percentage:</label>
                                    <input type="text" id="percentage" class="form-control" name="percentage" style="border-top-left-radius:10px;border-bottom-left-radius:10px;">
                                    <div class="input-group-append">
                                        <span class="input-group-text px-3" style="border-top-right-radius:9px;border-bottom-right-radius:9px;border:1px solid #d7dbda;background-color: #fff;">%</span>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary px-5 py-2">Add Coupon</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
