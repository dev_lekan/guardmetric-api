@extends('admin.inc.app')
@section('shipping')
    active
@endsection
@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            @include('admin.inc.notification')
            <div class="row">
                <div class="col-sm-12 px-0">
                    <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between align-items-center">
                            <div class="iq-header-title">
                                <h4 class="card-title font-weight-bold">Shipping</h4>
                            </div>
                            <div class="iq-header-title">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addProduct">
                                    Add Shipping Fee
                                </button>
                            </div>
                        </div>
                        <div class="iq-card-body pt-0">
                            <div class="table-responsive">
                                <table id="datatable" class="table table-striped table-hover" >
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Price</th>
                                        <th>Edit</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($states as $state)
                                        <tr>
                                            <td>{{$state->state}}</td>
                                            <td>₦{{$state->price}}</td>
                                            <td>
                                                <button data-toggle="modal" data-target="#state{{$state->id}}" class="btn btn-primary clickable"><i class="fa fa-edit mr-0" style="font-size: 17px"></i></button>
                                            </td>
                                        </tr>
                                        <div class="modal fade" id="state{{$state->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-modal="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalCenterTitle">Update Shipping Fee</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form method="post" action="/admin/update-shipping-fee">
                                                            @csrf
                                                            <input type="hidden" name="shipping_id" value="{{$state->id}}">
                                                            <div class="form-group">
                                                                <label for="state">State:</label>
                                                                <input type="text" class="form-control" id="state" name="state" value="{{$state->state}}">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="price">Price:</label>
                                                                <input type="text" class="form-control" id="price" name="price" value="{{$state->price}}">
                                                            </div>
                                                            <button type="submit" class="btn btn-primary px-5 py-2">Update Price</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Name</th>
                                        <th>Price</th>
                                        <th>Edit</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="addProduct" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-modal="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle">Add Shipping Fee</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="post" action="/admin/add-shipping-fee">
                                @csrf
                                <div class="form-group">
                                    <label for="state">State:</label>
                                    <input type="text" class="form-control" id="state" name="state">
                                </div>
                                <div class="form-group">
                                    <label for="price">Price:</label>
                                    <input type="text" class="form-control" id="price" name="price">
                                </div>
                                <button type="submit" class="btn btn-primary px-5 py-2">Add Fee</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
