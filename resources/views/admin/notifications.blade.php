@extends('admin.inc.app')
@section('notify')
    active
@endsection
@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            @include('admin.inc.notification')
            <div class="row">
                <div class="col-sm-12 px-0">
                    <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between align-items-center">
                            <div class="iq-header-title">
                                <h4 class="card-title font-weight-bold">Notification</h4>
                            </div>
                        </div>
                        <div class="iq-card-body pt-0">
                            <div class="table-responsive">
                                <table id="datatableF" class="table table-striped table-hover" >
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Date</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($msgs->reverse() as $msg)
                                        <tr>
                                            <td>{{$msg->name}}</td>
                                            <td>{{$msg->email}}</td>
                                            <td>{{date('D d,M Y H:i a', strtotime($msg->date))}}</td>
                                            <td>
                                                <a class="btn btn-primary clickable" href="/admin/notification/{{$msg->id}}"><i class="fa fa-arrow-right mr-0" style="font-size: 17px"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Date</th>
                                        <th></th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
