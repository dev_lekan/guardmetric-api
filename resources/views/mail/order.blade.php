<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Order details</title>
</head>
<body>
<div class="wrapper">
    <h4>Order Item</h4>
    <p>Below is your order details</p>
    <table border="1" cellspacing="0" cellpadding="5">
        <thead>
        <tr>
            <th>Products</th>
            <th>Quantity</th>
            <th>Price</th>
        </tr>
        </thead>
        <tbody>
        @foreach (json_decode($items) as $item)
            <tr>
                <td>{{$item->name}}</td>
                <td>{{$item->quantity}}</td>
                <td>₦{{$item->price}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <p>Shipping Fee: ₦{{$shipping}}</p>
    <p>Total: ₦{{$total}}</p>
    <p>Reference: {{$reference}}</p>
</div>
</body>
</html>
