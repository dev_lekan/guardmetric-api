<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::prefix('v1')->group(function () {
    Route::post('create-account',"loginController@createAccount");
    Route::post('login',"loginController@login");
    Route::post('forget-password',"loginController@forget_password");
    Route::post('reset-password',"loginController@reset_password");
    Route::get('verify-reset-token',"loginController@verify_password");
    Route::post('newsletter', 'loginController@newsletter');
    Route::post('send-message', 'loginController@sendMessage');

    Route::get('get-dashboard', 'userController@getDashboard');
    Route::post('/update-profile', 'userController@updateProfile');
    Route::post('/change-password', 'userController@changePassword');
    Route::post('/save-order', 'userController@saveOrder');
    Route::post('apply-coupon', 'userController@applyCoupon');
    Route::post('/verify-transaction', 'userController@verifyTransaction');

//    Products
    Route::get('products', "ProductController@products");
    Route::get('get-products/{number}', 'ProductController@getProducts');
    Route::get('product/{unique_id}', "ProductController@product");
    Route::get('category/{url_name}', "ProductController@category");
    Route::get('categories', "ProductController@categories");
//    Route::post('newsletterMail.blade.php', 'loginController@newsletterMail.blade.php');
    Route::get('get-featured', 'ProductController@getFeatured');
    Route::get('get-by-category', 'ProductController@getByFeatured');
    Route::get('get-top-categories', 'ProductController@getTopCategories');

    Route::get('search/{query}', 'ProductController@search');

    Route::get('get-shipping-fee', 'ProductController@getShippingFee');

});

