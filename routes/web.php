<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

Route::get('/', function(Request $request){
    $img = \App\Image::first();
    $name = explode('http://' . $request->getHttpHost() . '/image/', $img->name);
    dd($name);
//    $file="040521030654.jpg";
//    unlink(storage_path('/images/'.$file));
//    dd('deleted');
});

Route::namespace('Admin')->prefix('admin')->group(function(){
    Route::get('login', 'loginController@getLogin')->name('admin.login');
    Route::post('login', 'loginController@login');

    Route::middleware('auth:admin')->group(function(){
        Route::get('/', 'adminController@index');
        Route::get('orders', 'adminController@orders');
         Route::get('pending-orders', 'adminController@pendingOrders');
         Route::get('order/{id}', 'adminController@order');
         Route::post('deliver-order', 'adminController@deliverOrder');
         Route::post('cancel-order', 'adminController@cancelOrder');

        Route::get('users', 'adminController@users');
        Route::get('user/{id}', 'adminController@user');

        Route::get('categories', 'adminController@categories');
        Route::get('category/{id}', 'adminController@category');
        Route::post('add-category', 'adminController@addCategory');
        Route::post('update-category', 'adminController@updateCategory');
        Route::post('delete-category', 'adminController@deleteCategory');
        Route::post('feature-product', 'adminController@featureProduct');

        Route::get('coupons', 'adminController@coupons');
        Route::get('coupon/{id}', 'adminController@coupon');
        Route::post('create-coupon', 'adminController@createCoupon');
        Route::post('update-coupon', 'adminController@updateCoupon');
        Route::post('delete-coupon', 'adminController@deleteCoupon');

        Route::get('products', 'adminController@products');
        Route::get('product/{id}', 'adminController@product');
        Route::post('add-product', 'adminController@addProduct');
        Route::post('update-product', 'adminController@updateProduct');
        Route::post('delete-product', 'adminController@deleteProduct');
        Route::post('delete-product-image', 'adminController@deleteProductImage');
        Route::post('remove-product-color', 'adminController@removeProductColor');

        Route::post('add-shipping-fee', 'adminController@addShippingFee');
        Route::post('update-shipping-fee', 'adminController@updateShippingFee');
        Route::get('shipping-fee', 'adminController@shippingFee');

        Route::get('notifications', 'adminController@notifications');
        Route::get('notification/{id}', 'adminController@notification');

        Route::get('track-order', 'adminController@trackOrder');
        Route::post('get-order', 'adminController@getOrder');
        Route::post('logout', 'adminController@logout');

        Route::get('newsletter', 'NewsletterController@index');
        Route::post('send-newsletter', 'NewsletterController@newsLetter');
    });
});


Route::get('image/{filename}/{width?}/{height?}',function($filename,$width=0,$height=0){
    $img = Image::make(storage_path().'/images/'.$filename);
    if($width == 0 || $height == 0){
        $height=$img->height();
        $width=$img->width();
    }
    return $img->resize($width, $height)->response('jpg');
});
