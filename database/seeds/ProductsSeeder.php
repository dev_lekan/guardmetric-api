<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,15) as $index) {
            DB::table('products')->insert([
                'name' => $faker->name,
                'unique_id' => $faker->uuid,
                'category_id' => $faker->randomDigit(1,5),
                'price' => $faker->randomDigit(200, 1000),
                'quantity' => $faker->randomDigit(1, 50),
                'colors' => $faker->colorName,
                'description' => $faker->text,
                'status' => $faker->randomDigit(1, 2)
            ]);
        }
    }
}
