<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'username'=>'admin',
            'password'=>bcrypt('123456')
        ]);

//        $this->call(ProductsSeeder::class);
//        $faker = Faker::create();
//        foreach (range(1,15) as $index) {
//            DB::table('products')->insert([
//                'name' => $faker->name,
//                'unique_id' => $faker->uuid,
//                'category_id' => $faker->randomDigit(1,5),
//                'price' => $faker->randomDigit(200, 1000),
//                'quantity' => $faker->randomDigit(1, 50),
//                'colors' => json_encode([$faker->colorName]),
//                'description' => $faker->text,
//                'status' => $faker->randomDigit(1, 2)
//            ]);
//        }
    }
}
