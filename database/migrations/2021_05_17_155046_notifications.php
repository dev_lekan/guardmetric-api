<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Notifications extends Migration
{

    public function up()
    {
        Schema::create('notifications', function (Blueprint $table){
            $table->id();
            $table->string('email');
            $table->string('name');
            $table->string('body');
            $table->dateTime('date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
