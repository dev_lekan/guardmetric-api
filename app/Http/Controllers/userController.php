<?php

namespace App\Http\Controllers;

use App\Coupon;
use App\Orders;
use App\Products;
use App\Transaction;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use function Psy\sh;

class userController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    public function getDashboard(Request $request)
    {
        $user = Auth::user($request->only('token'));
        if (!$user) {
            return response()->json([
                'status' => false,
                'msg' => 'Token expired, pls login again'
            ]);
        }
        return response([
            'status' => true,
            'data' => array_merge(collect($user)->toArray()),
        ]);
    }

    public function saveOrder(Request $request){
        $user = Auth::user($request->only('token'));
        $this->validate($request, [
            'address'=>'required|string',
            'data'=>'required|string',
            'total'=>'required|regex:/^\d*(\.\d{2})?$/',
            'reference'=>'required|string',
        ]);
        $order = new Orders();
        $order->user_id = $user->id;
        $order->data = $request->data;
        $order->total = $request->total;
        $order->address=$request->address;
        $order->reference = $request->reference;
        if($request->note){
            $order->note = $request->note;
        }
        if(!$order->save()){
            return response([
                'status' => false,
                'msg' => 'Error saving order, please try again',
            ]);
        }
        return response([
            'status' => true,
            'msg' => 'Order saved successfully',
        ]);
    }

    public function updateProfile(Request $request)
    {
        $user = Auth::user($request->only('token'));
        $this->validate($request, [
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'email' => 'required',
            'country' => 'required|string',
            'state' => 'required|string',
            'city' => 'required|string',
            'zip' => 'required',
            'phone' => 'required',
        ]);
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->country = $request->country;
        $user->state = $request->state;
        $user->city = $request->city;
        $user->address = $request->address;
        $user->phone = $request->phone;
        $user->zip = $request->zip;
        if(!$user->save()){

            return response([
                'status' => false,
                'msg' => 'Error saving details',
            ]);
        }
        return response([
            'status' => true,
            'msg' => 'Details saved successfully',
        ]);

    }

    public function verifyTransaction(Request $request){
        $user = Auth::user($request->only('token'));
        $this->validate($request, [
            'reference'=>'required|string'
        ]);
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.paystack.co/transaction/verify/".$request->reference,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer ".env('P_KEY'),
                "Cache-Control: no-cache",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            return response([
                'status'=>false,
                'error'=>$err
            ]);
        } else {
            $resp = json_decode($response);
            if($resp->status){
                $order = Orders::where('reference', $request->reference)->first();
                $order->status = 1;
                $order->save();

                $trans = new Transaction();
                $trans->user_id = $user->id;
                $trans->order_id = $order->id;
                $trans->reference = $request->reference;
                $trans->status = 1;
                $trans->save();

                $shipping = 0;
                foreach (json_decode($order->data) as $data){
                    $sum = floatval($data->price) * floatval($data->quantity);
                    $shipping += $sum;
                }

                $shipping = $order->total - $shipping;

                $details = [
                    'items'=>$order->data,
                    'email'=>$user->email,
                    'shipping'=>$shipping,
                    'total'=>$order->total,
                    'reference'=>$request->reference,
                ];
                Mail::send('mail.order', $details, function ($m) use ($details){
                    $m->to($details['email'], 'Guardmetrics')->subject('Order Details');
                });

                return response([
                    'status'=>true,
                    'msg'=>'Payment verified'
                ]);
            }else{
                return response([
                    'status'=>false,
                    'msg'=>$resp->message
                ]);
            }

        }
    }

    public function applyCoupon(Request $request){
        $this->validate($request, [
            'code'=>'required|string'
        ]);
        $coupon = Coupon::where('code', $request->code)->first();
        if(!$coupon){
            return response(['status'=>false, 'msg'=>'Invalid coupon code']);
        }

        return response(['status'=>true, 'percentage'=>$coupon->percentage]);
    }

    public function changePassword(Request $request){
        $user = Auth::user($request->only('token'));
        $this->validate($request, [
            'old_password'=>'required|string',
            'password'=>'required|string|confirmed',
        ]);

        if (!Hash::check($request->old_password, $user->password)) {
            return response()->json([
                'status' => false,
                'msg' => 'Incorrect old password'
            ]);
        }

        $user->password = Hash::make($request->password);
        $user->save();
        return response([
            'status' => true,
            'msg' => 'Password changed successfully',
        ]);
    }
}
