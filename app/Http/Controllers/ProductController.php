<?php

namespace App\Http\Controllers;

use App\Category;
use App\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function getShippingFee(){
        $state = DB::table('state_shipping_fee')->get();
        return response()->json([
            'status'=>true,
            'data'=>$state
        ]);
    }

    public function search($query){
        $query = strval($query);
        $prods = Products::with('Category')
                        ->where('name','LIKE','%'.$query.'%')
                        ->orWhereHas('Category', function ($q) use ($query) {
                            $q->where('name', 'LIKE','%'.$query.'%');
                        })
                        ->get();
//        if(count($prods) < 1){
//            return response([
//                'status'=>false,
//                'msg'=>'No product found for your search'
//            ]);
//        }
        return response([
            'status'=>true,
            'data'=>$prods
        ]);
    }

    public function products(){
        $products = Products::paginate(12);

        return response([
            'status' => true,
            'products' => $products
        ]);
    }

    public function product($unique_id){
        $product = Products::where('unique_id', $unique_id)->first();

        return response([
            'status' => true,
            'product' => $product
        ]);
    }

    public function category($url){
        $category = Category::where('url_name', $url)->first();

        return response([
            'status' => true,
            'category' => $category
        ]);
    }

    public function getProducts($number){
        $products = Products::all()->sortByDesc('id')->take($number);
        return response()->json([
            'status'=>true,
            'products'=>$products->toArray()
        ]);
    }

    public function categories(){
        $category = Category::all()->sortByDesc('id');
        return response()->json([
            'status'=>true,
            'data'=>$category->toArray()
        ]);
    }

    public function getTopCategories(){
        $category = Category::all()->sortByDesc('id')->take(4);
        return response()->json([
            'status'=>true,
            'data'=>$category
        ]);
    }

    public function getFeatured(){
        $prods = Products::where('featured', 1)->get()->take(4);
        return response()->json([
            'status'=>true,
            'data'=>$prods
        ]);
    }

    public function getByFeatured(){
        $category = Category::all();
        return response([
            'status'=>true,
            'category'=>$category,
        ]);
    }
}
