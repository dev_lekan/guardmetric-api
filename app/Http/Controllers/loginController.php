<?php

namespace App\Http\Controllers;

use App\Mail\passwordReset;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Facades\JWTAuth;

class loginController extends Controller
{
    public function sendMessage(Request $request){
        $this->validate($request, [
            'email'=>'required|email',
            'name'=>'required|string',
            'body'=>'required|string'
        ]);
        DB::table('notifications')->insert([
            'email'=>$request->email,
            'name'=>$request->name,
            'body'=>$request->body,
            'date'=>now()
        ]);
        return response([
            'status'=>true,
            'msg'=>'Message sent successfully'
        ]);
    }

    public function login(Request $request){
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);
        $credentials = $request->only('email', 'password');
        if ($token = JWTAuth::attempt($credentials)) {
            $user = User::where('id', auth::user($request->only('token'))->id)->first();

            return response()->json([
                'status' => true,
                'msg' => "Login Successfull",
                'token' => $token,
                "user" => $user
            ],200);
        }else{
            return response([
                'status'=>false,
                'token'=>$request->only('token'),
                'msg'=>'Invalid login details'
            ]);
        }
    }

    public function createAccount(Request $request){
        $this->validate($request, [
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);

        $user = new User();
        $user->email = $request['email'];
        $user->password = bcrypt($request['password']);
        $user->remember_token = mt_rand(99999, 99999999) . Str::random(12) . mt_rand(99999, 99999999) . Str::random(12);
        $credentials = $request->only('email', 'password');

        $user->save();

        $news = DB::table('newsletter')->where('email', $user->email)->first();
        if(!$news){
            DB::table('newsletter')->insert([
                'email'=>$user->email,
                'created_at'=>now(),
                'updated_at'=>now(),
            ]);
        }

        if($token=JWTAuth::attempt($credentials)){
            $user=User::where('id',auth::user($token)->id)->first();
            $user->save();
            return response()->json([
                'status'=>true,
                'msg'=>"Your registration was successfull",
                'token'=>$token,
                "user"=>$user
            ]);
        }else{
            return response()->json(
                [
                    "status"=>false,
                    "message"=>"Invalid credentials details"
                ]
            );
        }
    }

    public function forget_password(Request $request){
        $this->validate($request, [
            'email' => 'required',
        ]);

        $user = User::where('email', $request->input('email'))->first();

        if (!$user) {
            return response()->json([
                'status' => false,
                'msg' => "Account not found"
            ]);
        }

        $user->remember_token = mt_rand(99999, 99999999) . Str::random(12) . mt_rand(99999, 99999999) . Str::random(12);
        $user->save();

        $link = env('APP_FRONTEND') . "/reset-password/" . $user->remember_token;
        Mail::to($user->email)->sendNow(new passwordReset([
            'link' => $link
        ]));

        return response()->json([
            'status' => true,
            'msg' => "Reset mail has been sent"
        ]);
    }

    public function verify_password(Request $request){
        $token = $request->query('code');

        if (!isset($token)) {
            return response()->json(['status' => false, 'msg' => "Token not found"]);
        }

        $user = User::where('remember_token', $token)->first();

        if (!$user) {
            return response()->json([
                'status' => false,
                'msg' => "Expired link"
            ]);
        }

        return response()->json(['status' => true, 'msg' => "correct token"]);
    }

    public function reset_password(Request $request){
        $this->validate($request, [
            'password' => 'required|confirmed',
            'token' => 'required'
        ]);

        $user = User::where('remember_token', $request->input('token'))->first();

        if (!$user) {
            return response()->json([
                'status' => false,
                'msg' => "Account not found"
            ]);
        }

        $user->password = bcrypt($request->input('password'));
        $user->remember_token = mt_rand(99999, 99999999) . Str::random(12) . mt_rand(99999, 99999999) . Str::random(12);
        $user->save();

        return response()->json(['status' => true, 'msg' => "Password reset was successful"]);
    }

    public function newsletter(Request $request){
        $this->validate($request, [
            'email'=>'required|email'
        ]);
        $email = DB::table('newsletter')->where('email', $request->email)->first();
        if($email){
            return response()->json(['status'=>false, 'msg'=>'Email already exist']);
        }

        DB::table('newsletter')->insert([
            'email'=>$request->email
        ]);
        return response()->json(['status'=>true, 'msg'=>'Email added successfully']);
    }
}
