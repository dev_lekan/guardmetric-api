<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\helpController;
use App\Jobs\NewsletterMailJob;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class NewsletterController extends Controller
{
    public function index(){
        $subscribers = Db::table('newsletter')->get();

        return view('admin.newsletter.index')->with(['subscribers' => $subscribers]);
    }

    public function newsLetter(Request $request){
        $this->validate($request, [
            'subject'=>'required|string',
            'message'=>'required|string'
        ]);

        $emails = Db::table('newsletter')->get('email');

        $send=[];
        foreach($emails as $email){

            if(!in_array($email, $send)){
                array_push($send,$email);
            }
        }

        if(count($send) < 1){
            helpController::flashSession(false, 'Mail list is empty, pls try again');
            return redirect()->back();
        }

        $data=['subject'=>$request->subject, 'msg'=>strval($request->message),'emails'=>$send ];

        NewsletterMailJob::dispatchAfterResponse($data);
        helpController::flashSession(true, 'Announcement sent successfully');
        return redirect()->back();
    }
}
