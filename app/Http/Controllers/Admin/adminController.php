<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Coupon;
use App\Http\Controllers\Controller;
use App\Http\Controllers\helpController;
use App\Orders;
use App\Products;
use App\Image;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use function Symfony\Component\String\b;

class adminController extends Controller
{
    public function index(){
        $orders = Orders::where('status','!=', 0)->count();
        $delivered = Orders::where('status', 2)->count();
        $products = Products::all()->count();
        $category = Category::all()->count();
        return view('admin.dashboard')->with(['category'=>$category,'products'=>$products,'orders'=>$orders,'delivered'=>$delivered]);
    }

    public function deliverOrder(Request $request){
        $this->validate($request, [
            'order_id'=>'required|integer'
        ]);
        $ord = Orders::find($request->order_id);
        if(!$ord){
            helpController::flashSession(false, 'Order not found');
            return back();
        }
        $ord->status = 2;
        if(!$ord->save()){
            helpController::flashSession(false, 'Error verifing order');
            return back();
        }
        helpController::flashSession(true, 'Order verified successfully');
        return back();
    }

    public function cancelOrder(Request $request){
        $this->validate($request, [
            'order_id'=>'required|integer'
        ]);
        $ord = Orders::find($request->order_id);
        if(!$ord){
            helpController::flashSession(false, 'Order not found');
            return back();
        }
        $ord->status = -1;
        if(!$ord->save()){
            helpController::flashSession(false, 'Error canceling order');
            return back();
        }
        helpController::flashSession(true, 'Order cancelled successfully');
        return back();
    }

    public function pendingOrders(){
        //show only pending orders
        $orders = Orders::where('status', 1)
            ->orderBy('created_at', 'desc')
            ->get();
        return view('admin.orders.pending')->with('orders', $orders);
    }

    public function notifications(){
        $msgs = DB::table('notifications')->get();
        return view('admin.notifications', ['msgs'=>$msgs]);
    }

    public function notification($id){
        $msg = DB::table('notifications')->where('id', $id)->first();
        return view('admin.notification', ['msg'=>$msg]);
    }

    public function users(){
        $users = User::all();
        return view('admin.users.users', ['users'=>$users]);
    }

    public function user($id){
        $user = User::findOrFail($id);
        dd($user->orders);
        return view('admin.user', ['user'=>$user]);
    }

    public function shippingFee(){
        $state=DB::table('state_shipping_fee')->get();
        return view('admin.shipping_fee', ['states'=>$state]);
    }

    public function createCoupon(Request $request){
        $this->validate($request, [
            'code'=>'required|string|unique:coupons',
            'percentage'=>'required|regex:/^\d*(\.\d{2})?$/'
        ]);

        $coup = new Coupon();
        $coup->code = $request->code;
        $coup->percentage = $request->percentage;
        if(!$coup->save()){
            helpController::flashSession(false, 'Error creating coupon, please try again');
            return back();
        }
        helpController::flashSession(true, 'Coupon created successfully');
        return back();
    }

    public function coupons(){
        $coupons = Coupon::all();
        return view('admin.coupons', ['coupons'=>$coupons]);
    }

    public function updateCoupon(Request $request){
        $this->validate($request, [
            'id'=>'required|integer',
            'percentage'=>'required|regex:/^\d*(\.\d{2})?$/'
        ]);
        $coupon = Coupon::find($request->id);
        if(!$coupon){
            helpController::flashSession(false, 'Coupon not found');
            return back();
        }
        $coupon->code = $request->code;
        $coupon->percentage = $request->percentage;
        if(!$coupon->save()){
            helpController::flashSession(false, 'Error updating coupon, please try again');
            return back();
        }
        helpController::flashSession(true, 'Coupon updated successfully');
        return back();
    }

    public function coupon($id){
        $coupon = Coupon::find($id);
        if(!$coupon){
            helpController::flashSession(false, 'Coupon not found');
            return back();
        }

        return view('admin.coupon', ['coupon'=>$coupon]);
    }

    public function deleteCoupon(Request $request){
        $this->validate($request, [
            'id'=>'required|integer'
        ]);
        $coupon = Coupon::find($request->id);
        if(!$coupon){
            helpController::flashSession(false, 'Coupon not found');
            return back();
        }
        if(!$coupon->delete()){
            helpController::flashSession(false, 'Error deleting coupon');
            return back();
        }
        helpController::flashSession(true, 'Coupon deleted successfully');
        return redirect('admin/coupons');
    }

    public function addShippingFee(Request $request){
        $this->validate($request, [
            'state'=>'required|unique:state_shipping_fee,state',
            'price'=>'required|regex:/^\d*(\.\d{2})?$/'
        ]);
        DB::table('state_shipping_fee')->insert([
            'state'=>$request->state,
            'price'=>$request->price
        ]);
        helpController::flashSession(true, 'Shipping fee add successfully');
        return back();
    }

    public function updateShippingFee(Request $request){
        $this->validate($request, [
            'shipping_id'=>'required|integer',
            'price'=>'required|regex:/^\d*(\.\d{2})?$/'
        ]);
        DB::table('state_shipping_fee')->where('id',$request->shipping_id)->update([
            'price'=>$request->price
        ]);
        helpController::flashSession(true, 'Shipping fee updated successfully');
        return back();
    }

    public function products(){
        $products = Products::all();
        $category = Category::all();
        $json = Storage::disk('local')->get('colors.json');
        $colors = json_decode($json, true);
//        dd($products[0]['images']);
        return view('admin.products.index')->with(['products'=>$products, 'categorys'=>$category, 'colors'=>$colors]);
    }

    public function product($id){
        $product = Products::find($id);
        $category=Category::all();
        if(!$product){
            helpController::flashSession(false, 'Product not found');
            return redirect('/admin/products');
        }
        $json = Storage::disk('local')->get('colors.json');
        $colors = json_decode($json, true);
        return view('admin.products.product')->with(['product'=>$product, 'categories'=>$category,'colors'=>$colors]);
    }

    public function deleteProductImage(Request $request){
        $this->validate($request, [
            'id'=>'required|integer'
        ]);
        $image = Image::find($request->id);
        if(!$image){
            helpController::flashSession(false, 'Image not found');
            return back();
        }

        $name = explode('http://' . $request->getHttpHost() . '/image/', $image->name)[1];
        unlink(storage_path('/images/'.$name));

        if(!$image->delete()){
            helpController::flashSession(false, 'Error deleting image');
            return back();
        }
        helpController::flashSession(true, 'Image deleted successfully');
        return back();
    }

    public function removeProductColor(Request $request){
        $this->validate($request, [
            'product_id'=>'required|integer',
            'color'=>'required'
        ]);
        $prod = Products::find($request->product_id);
        if(!$prod){
            helpController::flashSession(false, 'Product not found');
            return back();
        }
        $colors = json_decode($prod->colors);
        array_splice($colors,array_search($request->color,$colors),1);
        $prod->colors=json_encode($colors);
        if(!$prod->save()){
            helpController::flashSession(false, 'Error removing color');
            return back();
        }
        helpController::flashSession(true, 'Color removed successfully');
        return back();
    }

    public function updateProduct(Request $request){
        $this->validate($request, [
            'product_id'=>'required|integer',
            'name'=>'required|string',
            'price'=>'required|regex:/^\d*(\.\d{2})?$/',
            'description'=>'required|string',
            'category_id'=>'required|integer',
        ]);

        $prod = Products::find($request->product_id);
        if(!$prod){
            helpController::flashSession(false, 'Product not found');
            return back();
        }
        $colors=json_decode($prod->colors);
        if($request->colors){
            foreach($request->colors as $color){
                array_push($colors, $color);
            }
        }
        $prod->name = $request->name;
        $prod->price= $request->price;
        $prod->category_id=$request->category_id;
        $prod->description = $request->description;
        $prod->colors=json_encode(array_unique($colors));
        $request->set ? $prod->status = 0 : $prod->status = 1;

        if ($request->hasFile("images")) {
            $files = $request->file('images');
            foreach ($files as $file) {
                $formats = ['jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG'];

                if (!in_array($file->getClientOriginalExtension(), $formats)) {
                    helpController::flashSession(false, 'File format not supported');
                    return back();
                }

                $imagename = date('dmyhis').substr(str_shuffle("0123456789abcdefghijklmnopqrstvwxyz"), 0, 5).'.'.$file->getClientOriginalExtension();;

                $file->move(storage_path() . "/images/", $imagename);
                $imagename = "http://" . $request->getHttpHost() . "/image/" . $imagename;
                $image = new Image();
                $image->products_id = $request->product_id;
                $image->name = $imagename;
                $image->save();
            }
//            $this->saveMultipleImage($request, $request->id);
        }

        if(!$prod->save()){
            helpController::flashSession(false, 'Error updating products');
            return back();
        }
        helpController::flashSession(true, 'Product updated successfully');
        return back();
    }

    public function addProduct(Request $request){
        $this->validate($request, [
            'name'=>'required|string',
            'price'=>'required|regex:/^\d*(\.\d{2})?$/',
            'category_id'=>'required|integer',
            'colors'=>'required',
            'quantity'=>'required|integer',
            'description'=>'required|string',
        ]);
        $colors=[];
        foreach($request->colors as $color){
            array_push($colors, $color);
        }
        $prod = new Products();
        $prod->name = $request->name;
        $prod->unique_id = substr(str_shuffle("0123456789abcdefghijklmnopqrstvwxyz"), 0, 7);
        $prod->category_id = $request->category_id;
        $prod->price = $request->price;
        $prod->quantity=$request->quantity;
        if($request->features){
            $prod->features = $request->features;
        }
        $prod->description = $request->description;
        $prod->colors = json_encode($colors);
        $prod->save();
        if ($request->hasFile("images")) {
            $files = $request->file('images');
            foreach ($files as $file) {
                $formats = ['jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG'];

                if (!in_array($file->getClientOriginalExtension(), $formats)) {
                    helpController::flashSession(false, 'File format not supported');
                    return back();
                }

                $imagename = date('dmyhis').substr(str_shuffle("0123456789abcdefghijklmnopqrstvwxyz"), 0, 5).'.'.$file->getClientOriginalExtension();

                $file->move(storage_path() . "/images/", $imagename);
                $imagename = "http://" . $request->getHttpHost() . "/image/" . $imagename;
                $image = new Image();
                $image->products_id = $prod->id;
                $image->name = $imagename;
                $image->save();
            }
        }else{
            $prod->delete();
            helpController::flashSession(false, 'Image field is required');
            return back();
        }
        helpController::flashSession(true, 'Product saved successfully');
        return back();
    }

    public function featureProduct(Request $request){
        $this->validate($request, [
            'product_id'=>'required|integer'
        ]);
        $prod=Products::find($request->product_id);
        if(!$prod){
            helpController::flashSession(false, 'Product not found');
            return back();
        }
        if($prod->featured == 0){
            $prod->featured = 1;
            $msg = 'Product featured successfully';
        }else{
            $prod->featured = 0;
            $msg = 'Product removed from featured successfully';
        }
        if(!$prod->save()){
            helpController::flashSession(false, 'Error updating product, please try again');
            return back();
        }
        helpController::flashSession(true, $msg);
        return back();

    }

    public function addCategory(Request $request){
        $this->validate($request, [
            'name'=>'required|string',
        ]);

        $category = new Category();
        $category->name = $request->name;
        $category->url_name =strtolower(str_replace(' ', '_', trim($request->name)));
        if ($request->hasFile("image")) {
            $file = $request->file('image');
            $formats = ['jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG'];

            if (!in_array($file->getClientOriginalExtension(), $formats)) {
                helpController::flashSession(false, 'File format not supported');
                return back();
            }

            $imagename = date('dmyhis').'.'.$file->getClientOriginalExtension();

            $file->move(storage_path() . "/images/", $imagename);
            $category->image = "http://" . $request->getHttpHost() . "/image/" . $imagename;
        }else{
            helpController::flashSession(false, 'Image field is required');
            return back();
        }

        if(!$category->save()){
            helpController::flashSession(false, 'Error saving category');
            return back();
        }
        helpController::flashSession(true, 'Category saved successfully');
        return back();
    }

    public function deleteProduct(Request $request){
        $this->validate($request, [
            'product_id'=>'required|integer',
        ]);
        $product = Products::find($request->product_id);
        if(!$product){
            helpController::flashSession(false, 'Product not found');
            return back();
        }
        $imgs = Image::where('products_id', $request->product_id)->get();
        foreach ($imgs as $img){
            $name = explode('http://' . $request->getHttpHost() . '/image/', $img->name)[1];
            unlink(storage_path('/images/'.$name));
            $img->delete();
        }

        if(!$product->delete()){
            helpController::flashSession(false, 'Error deleting product');
            return back();
        }
        helpController::flashSession(true, 'Product deleted successfully');
        return redirect('/admin/products');
    }

    public function orders(){
        $orders = Orders::where('status', 1)->orderBy('created_at', 'desc')->get();
        return view('admin.orders.index')->with('orders', $orders);
    }

    public function order($id){
        $order = Orders::findOrFail($id);
        return view('admin.orders.order')->with('order', $order);
    }

    public function logout(Request $request){
        Auth::guard('admin')->logout();
        return redirect('admin/login');
    }

    public function categories(){
        $category = Category::all();
        return view('admin.category.index')->with('category',$category);
    }

    public function category($id){
        $cat = Category::find($id);
        if(!$cat){
            helpController::flashSession(false, 'Category not found');
            return redirect('/admin/categories');
        }
        return view('admin.category.category')->with('category', $cat);
    }

    public function updateCategory(Request $request){
        $this->validate($request, [
            'id'=>'required|integer',
            'name'=>'required|string',
        ]);

        $category = Category::find($request->id);
        if(!$category){
            helpController::flashSession(false, 'Category not found');
            return back();
        }

        $category->name = $request->name;
        $category->url_name =strtolower(str_replace(' ', '_', trim($request->name)));
        if ($request->hasFile("image")) {
            $file = $request->file('image');
            $formats = ['jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG'];

            if (!in_array($file->getClientOriginalExtension(), $formats)) {
                helpController::flashSession(false, 'File format not supported');
                return back();
            }

            $imagename = date('dmyhis').'.'.$file->getClientOriginalExtension();

            $file->move(storage_path() . "/images/", $imagename);
            $category->image = "http://" . $request->getHttpHost() . "/image/" . $imagename;
        }
        if(!$category->save()){
            helpController::flashSession(false, 'Error updating category');
            return back();
        }
        helpController::flashSession(true, 'Category updated successfully');
        return back();
    }

    public function deleteCategory(Request $request){
        $this->validate($request, [
            'id'=>'required|integer'
        ]);
        $category = Category::find($request->id);
        if(!$category){
            helpController::flashSession(false, 'Category not found');
            return redirect('/admin/categories');
        }
        $products = Products::where('category_id', $request->id)->delete();
        if(!$category->delete()){
            helpController::flashSession(false, 'Error deleting category');
            return back();
        }
        helpController::flashSession(true, 'Category deleted successfully');
        return redirect('/admin/categories');
    }

}
