<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Products extends Model
{
    protected $appends=['category','images'];
    public function category(){
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function images(){
        return $this->hasMany(Image::class);
    }

    public function getCategoryAttribute(){
        return $this->category()->first()->toArray();
    }
    public function getImagesAttribute(){
        return $this->images()->get();
    }
}
