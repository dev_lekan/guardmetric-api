<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $appends=['items'];

    public function getItemsAttribute(){
        $data = json_decode($this->data);
        return $data;
    }

//    public function getAttribute(){
//        $data = json_decode($this->data);
//        return $data;
//    }

    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }

}
