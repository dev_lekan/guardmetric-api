<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $appends=['orders','delivery'];

    protected $fillable = [
        'name', 'email', 'password',
    ];

    public function getOrdersAttribute(){
        return $this->orders()->where('status','!=', 0)->get();
    }

    public function getDeliveryAttribute(){
        return $this->orders()->where('status', 2)->count();
    }

    public function orders(){
        return $this->hasMany(Orders::class);
    }

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {

        return [];
    }
}
