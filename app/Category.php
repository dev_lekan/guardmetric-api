<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Category extends Model
{
    protected $table = "categories";
    protected $appends =['some_products','products'];

    public function getSomeProductsAttribute(){
        $prod=DB::table('products')->where('category_id', $this->id)->get()->take(3);
        foreach ($prod as $value){
            $img = Image::where('products_id', $value->id)->first();
            if($img){
                $value->image=$img->name;
            }
        }
        return $prod;
    }

    public function getProductsAttribute(){
        $products=DB::table('products')->where('category_id', $this->id)->get();
        foreach ($products as $value){
            $img = Image::where('products_id', $value->id)->first();
            if($img){
                $value->image=$img->name;
            }
        }
        return $products;
    }

//    public function products(){
//        return $this->hasMany(Products::class);
//    }
}
